module Main where

import Control.Monad
import Control.Monad.Trans (liftIO)
import Control.Monad.Writer.Strict (execWriterT, tell)
import qualified Data.ByteString.Char8 as BS
import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Data.HashMap.Strict as HM
import Data.Maybe
import Data.Monoid
import Data.Aeson (withObject)
import Data.Yaml
import Options.Applicative
import System.Exit
import qualified Utils.Interpol as I -- TODO: Rewrite in Attoparsec
import System.Process (createProcess, shell, waitForProcess)

data Options = Options
    { optPath :: String
    , optQuiet :: Bool
    } deriving (Show)

main :: IO ()
main = execParser opts >>= run
  where
    opts = info (helper <*> parser)
                ( fullDesc
                )

    parser = Options
        <$> argument str (metavar "TEMPLATE")
        <*> switch
            (  long "quiet"
            <> short 'q'
            <> help "Do nice things then run"
            )

data Protocol = Unknown String String
              | LocalFile FilePath
    deriving (Show)

run :: Options -> IO ()
run opts = process opts $
    case drop 1 <$> break (== ':') (optPath opts) of
        (path, "")     -> LocalFile path
        ("file", path) -> LocalFile path
        (proto, path)  -> Unknown proto path

process :: Options -> Protocol -> IO ()
process _ (Unknown proto path) = putStrLn $ mconcat
    [ "Unknown protocol "
    , show proto
    , " for path "
    , show path
    ]

process opts (LocalFile path) = do
    print path
    contents <- BS.readFile path
    BS.putStrLn contents
    decodeTpl opts contents

decodeTpl :: Options -> BS.ByteString -> IO ()
decodeTpl opts contents =
    case decodeEither contents of
        Left err  -> putStrLn $ "Template decoding error: " <> err
        Right tpl -> runTemplate opts tpl

runTemplate :: Options -> Template -> IO ()
runTemplate opts tpl = do
    T.putStrLn $ tTitle tpl
    case tDescr tpl of
        Nothing -> return ()
        Just descr -> putStrLn "" >> T.putStrLn descr >> putStrLn ""
    
    putStrLn $ "Source: " <> show (tSource tpl)

    when (null $ tParams tpl) $ do
        putStrLn ""
        putStrLn "Do you wish to continue? [Y/n]"
        getLine >>= \case
            ('y':_) -> return ()
            ('Y':_) -> return ()
            ("")    -> return ()
            _       -> exitSuccess

    params <- collectParams opts (tParams tpl)
    print params

    runCommands opts params (tSteps tpl)

collectParams :: Options -> [Param] -> IO (HM.HashMap T.Text T.Text)
collectParams _ ps =
    execWriterT $ forM_ ps $ \param -> do
        val <- liftIO $ do
            T.putStrLn $ fromMaybe (pKey param) (pTitle param)

            maybe (return ()) T.putStrLn (pHelp param)

            T.putStrLn . mconcat $
                [ "["
                , pDefault param
                , "]:"
                ]

            T.getLine >>= \case
                "" -> return $ pDefault param
                v  -> return v

        tell $ HM.singleton (pKey param) val

runCommands :: Options -> HM.HashMap T.Text T.Text -> [Step] -> IO ()
runCommands _ params steps = forM_ steps $ \step -> do
    let interpol = I.interpolate params
    case step of
        Run cmd -> do
            let cmdi = interpol cmd
            (_, _, _, ph) <- createProcess (shell $ T.unpack cmdi)
            waitForProcess ph >>= \case
                ExitSuccess   -> return ()
                ExitFailure _ -> exitFailure

        Edit fpath -> do
            let fpathi = T.unpack $ interpol fpath
            print ("Edit", fpathi)
            text <- T.readFile fpathi
            let texti = interpol text
            T.writeFile fpathi texti

data Template = Template
    { tTitle  :: T.Text
    , tDescr  :: Maybe T.Text
    , tSource :: Maybe Source
    , tParams :: [Param]
    , tSteps  :: [Step]
    } deriving (Show)

instance FromJSON Template where
    parseJSON = withObject "Template" $ \o -> Template
        <$> o .: "title"
        <*> o .:? "descr"
        <*> (parseSource <$> o .:? "source")
        <*> o .:? "params" .!= []
        <*> o .:? "steps"  .!= []

parseSource :: Maybe T.Text -> Maybe Source
parseSource = fmap parse
  where
    parse s =
        case fmap (T.drop 1) (T.breakOn ":" s) of
            (path, "")     -> LocalCopy path
            ("file", path) -> LocalCopy path
            ("gist", path) -> GistRepo path
            (proto, path)  -> error $ "Unknown source: " <> show s

data Source = LocalCopy T.Text
            | GistRepo T.Text
    deriving (Show)

data Param = Param
    { pKey     :: T.Text
    , pTitle   :: Maybe T.Text
    , pHelp    :: Maybe T.Text
    , pDefault :: T.Text
    -- TODO: variants
    } deriving (Show)

instance FromJSON Param where
    parseJSON = withObject "Param" $ \o -> Param
        <$> o .: "key"
        <*> o .:? "title"
        <*> o .:? "help"
        <*> o .:? "default" .!= ""

data Step = Run T.Text
          | Edit T.Text
    deriving (Show)

instance FromJSON Step where
    parseJSON (Data.Yaml.String cmd) =
        return $ Run cmd
    
    parseJSON obj@(Data.Yaml.Object is) = withObject "StepObject" parseStep obj
        where
            parseStep o =
                    parseRun o
                <|> parseEdit o
                <|> fail ("Unable to parse step - " <> show (HM.toList is))

            parseRun o = Run
                <$> o .: "run"

            parseEdit o = Edit
                <$> o .: "edit"

    parseJSON other = fail $ "Unexpected step input - " <> show other
